//
//  Literals.swift
//  Interpreter
//
//  Created by Varshan Srivasanthakumar on 2019-03-01.
//  Copyright © 2019 Varshan Srivasanthakumar. All rights reserved.
//

import Foundation

enum LiteralTypes {
    case String
    case Double
    case Integer
    case Identifier
    case Expression
}

protocol Literal {
    var type: LiteralTypes { get }
    func eval() -> AnyObject
}

class StringLiteral: Literal {
    var type = LiteralTypes.String
    var literal: String
    init(literal: String) {
        self.literal = literal
    }
    func eval() -> AnyObject {
        return literal as AnyObject
    }
}

class DoubleLiteral: Literal {
    var type = LiteralTypes.Double
    var literal: Double
    init(literal: Double) {
        self.literal = literal
    }
    func eval() -> AnyObject {
        return literal as AnyObject
    }
}

class IntegerLiteral: Literal {
    var type = LiteralTypes.Double
    var literal: Int
    init(literal: Int) {
        self.literal = literal
    }
    func eval() -> AnyObject {
        return literal as AnyObject
    }
}

class EmbeddedExpressionLiteral: Literal {
    var type = LiteralTypes.Expression
    var literal: Expression
    init(literal: Expression) {
        self.literal = literal
    }
    func eval() -> AnyObject {
        return try! literal.eval() as AnyObject
    }
}

class retrieveIdentifier {
    var type = LiteralTypes.Identifier
    var literal: String
    init(literal: String) {
        self.literal = literal
    }
    func eval() -> AnyObject? {
        return symbolTable[literal]
    }
}
