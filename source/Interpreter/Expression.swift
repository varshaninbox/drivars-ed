//
//  Expression.swift
//  Test
//
//  Created by Varshan Srivasanthakumar on 2019-04-25.
//  Copyright © 2019 Varshan Srivasanthakumar. All rights reserved.
//

import Foundation

class Expression {
    var op: expressionOp?
    var term: Term
    var term2: Term?
    var type: LiteralTypes?
    init(op: expressionOp? = nil, term: Term, term2: Term? = nil) {
        self.op = op
        self.term = term
        self.term2 = term2
        if (term2 != nil) {
            if ((term.type == LiteralTypes.String ||
                term2!.type == LiteralTypes.String) &&
                op! != expressionOp.Addition) {
                type = term.type
            } else if (term.type == term2!.type) {
                type = term.type
            } else if (term2!.type != term.type) {
                type = LiteralTypes.Double
            }
        } else {
            type = term.type
        }
    }
    func eval() throws -> AnyObject {
        /* If there is a second factor */
        if (term2 != nil && op != nil) {
            if (self.term.type == LiteralTypes.String &&
                self.term2!.type == LiteralTypes.String &&
                self.op! == expressionOp.Addition) {
                return (try self.term.eval() as! String) + (try self.term2!.eval() as! String) as AnyObject
            } else if (self.term.type == LiteralTypes.String ||
                self.term2!.type == LiteralTypes.String) {
                throw ExpressionTypeError.NoStringOperations
            } else if (self.term.type == LiteralTypes.Integer &&
                self.term2!.type == LiteralTypes.Integer) {
                return expressionIntBinaryOp(op: self.op!, a: try self.term.eval() as! Int, b: try self.term2!.eval() as! Int) as AnyObject
            } else if (self.term.type == LiteralTypes.Double &&
                self.term2!.type == LiteralTypes.Double) {
                return expressionDoubleBinaryOp(op: self.op!, a: try self.term.eval() as! Double, b: try self.term2!.eval() as! Double) as AnyObject
            } else {
                return expressionDoubleBinaryOp(op: self.op!, a: try self.term.eval() as! Double, b: try self.term2!.eval() as! Double) as AnyObject
            }
        } else {
            return try term.eval()
        }
    }
}

class Term {
    var op: termOp?
    var factor: Factor
    var factor2: Factor?
    var type: LiteralTypes?
    init(op: termOp? = nil, factor: Factor, factor2: Factor? = nil) {
        self.op = op
        self.factor = factor
        self.factor2 = factor2
        if (factor2 != nil) {
            if (factor.type == LiteralTypes.String ||
                factor2!.type == LiteralTypes.String) {
                type = nil
            } else if (factor2!.type == factor.type) {
                type = factor.type
            } else if (factor2!.type != factor.type) {
                type = LiteralTypes.Double
            }
        } else {
            type = factor.type
        }
    }
    func eval() throws -> AnyObject {
        /* If there is a second factor */
        if (factor2 != nil && op != nil) {
            if (self.factor.type == LiteralTypes.String ||
                self.factor2!.type == LiteralTypes.String) {
                throw TermTypeError.NoStringOperations
            } else if (self.factor.type == self.factor2!.type &&
                self.factor.type == LiteralTypes.Integer) {
                return termIntBinaryOp(op: self.op!, a: self.factor.eval() as! Int, b: self.factor2!.eval() as! Int) as AnyObject
            } else {
                return termDoubleBinaryOp(op: self.op!, a: self.factor.eval() as! Double, b: self.factor2!.eval() as! Double) as AnyObject
            }
        } else {
            return factor.eval()
        }
    }
}

class Factor {
    var type: LiteralTypes
    var literal: Literal
    init(literal: Literal) {
        self.literal = literal
        self.type = literal.type
    }
    func eval() -> AnyObject {
        return literal.eval()
    }
}
