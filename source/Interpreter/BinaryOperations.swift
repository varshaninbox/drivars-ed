//
//  BinaryOperations.swift
//  Interpreter
//
//  Created by Varshan Srivasanthakumar on 2019-04-20.
//  Copyright © 2019 Varshan Srivasanthakumar. All rights reserved.
//

import Foundation

enum expressionOp {
    case Addition
    case Subtraction
}

enum termOp {
    case Multiplication
    case Division
}

func expressionDoubleBinaryOp(op: expressionOp, a: Double, b: Double) -> Double {
    if (op == expressionOp.Addition) {
        return a + b
    } else {
        return a - b
    }
}

func expressionIntBinaryOp(op: expressionOp, a: Int, b: Int) -> Int {
    if (op == expressionOp.Addition) {
        return a + b
    } else {
        return a - b
    }
}

func termDoubleBinaryOp(op: termOp, a: Double, b: Double) -> Double {
    if (op == termOp.Multiplication) {
        return a * b
    } else {
        return a / b
    }
}

func termIntBinaryOp(op: termOp, a: Int, b: Int) -> Int {
    if (op == termOp.Multiplication) {
        return a * b
    } else {
        return a / b
    }
}
