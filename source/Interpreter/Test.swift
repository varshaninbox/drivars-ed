//
//  Test.swift
//  Test
//
//  Created by Varshan Srivasanthakumar on 2019-04-25.
//  Copyright © 2019 Varshan Srivasanthakumar. All rights reserved.
//

import Foundation

let test1 = IntegerLiteral(literal: 3)
let test2 = DoubleLiteral(literal: 3)
let test3 = Term(op: termOp.Multiplication, factor: Factor(literal: test1), factor2: Factor(literal: test1))
let test4 = Term(op: termOp.Multiplication, factor: Factor(literal: test1), factor2: Factor(literal: test1))
let test5 = Expression(op: expressionOp.Addition, term: test3, term2: test4)
