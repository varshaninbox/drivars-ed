//
//  TypeError.swift
//  Test
//
//  Created by Varshan Srivasanthakumar on 2019-04-25.
//  Copyright © 2019 Varshan Srivasanthakumar. All rights reserved.
//

import Foundation

enum TermTypeError: Error {
    case NoStringOperations
}

enum ExpressionTypeError: Error {
    case NoStringOperations
}

enum StatementTypeError: Error {
    case NoIdentifier
}
