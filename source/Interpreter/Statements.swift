//
//  Statements.swift
//  Interpreter
//
//  Created by Varshan Srivasanthakumar on 2019-03-13.
//  Copyright © 2019 Varshan Srivasanthakumar. All rights reserved.
//

import Foundation

var symbolTable: [String:AnyObject] = [:]

enum StatementType {
    case Assignment
    case Declaration
    case ReturnStatement
    case IfStatement
    case WhileLoop
    case SelectorStatement
}

class Statement {
    var type: StatementType
    var identifier: String?
    var assignment: Expression?
    init(type: StatementType, identifier: String? = nil, assignment: Expression? = nil) {
        self.type = type
        switch type {
        case StatementType.Assignment:
            if identifier != nil && assignment != nil {
                symbolTable[identifier!] = assignment
            }
        default:
            break
        }
    }
    func eval() throws -> AnyObject? {
        return nil
    }
}
