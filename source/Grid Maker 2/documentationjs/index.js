/*
	Copyright (c) 2019 Varshan Srivasanthakumar

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
*/

/* stores the instruction data */
var instructionCount = 1;
var instructionDictionary = {};

/* stores the grid data */
var grid = {};
var gridIdentifiers = {};
var gridDirections = {};

/* stores the basic map types */
var mapTypes = {
    0: "",
    1: "S",
    2: "T",
    3: "R",
    4: "🛑"
};

/* selected type is for toolbox,
 * selectedMode is for the edit mode toggle */
var selectedType = 0;
var selectedMode = 0;

/* The start and end positions on the grid */
var startPosition = {};
var endPosition = {};
var lastSquare = {};

/* The console outputs as a string array */
var consoleOutput = [];

/* The default settings of the lesson */
var lesson = {
    'type': 0,
    'autoexam': 1,
    'draft': 1
};

/* To prevent double compilation */
var compilingInProgress = false;

/* initialize code mirrors */
var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
    lineNumbers: true
});

var editorGlobal = CodeMirror.fromTextArea(document.getElementById("code-global"), {
    lineNumbers: true
});

var consoleView = CodeMirror.fromTextArea(document.getElementById("console-log-view"), {
    lineNumbers: true,
    readOnly: false
});

/* initialize split screens */
Split(['#grid-panel', '#square-panel'], {
    sizes: [70, 30],
    minSize: 250
});

/* refreshes the code mirrors on view appear */
$('a[data-toggle="tab"]').on("show.bs.tab", function(e) {
    if (e.target.getAttribute("data-tab-type") == "console-view") {
        consoleView.refresh();
        consoleView.setValue(consoleOutput.join("\n"));
        return;
    }

    if (e.target.getAttribute("data-tab-type") == "global-variables") {
        editorGlobal.refresh();
        return;
    }

    if (e.target.getAttribute("data-tab-type") == "actions") {
        editor.refresh();
        return;
    }
});

/**
 * This function adds an instruction step.
 * @param {e} to capture click event
 */
$('#add-step-btn').on("click", function(e) {
    var instDesc = $("#instruction-description").val();
    var instImage = $("#instruction-image").val();
    var instli = document.createElement("li");

    if (instDesc == "") {
        /* return on empty description */
        return;
    }

    /* make the dynamically added dom elements pretty */
    instli.setAttribute("class", "card");
    instli.setAttribute("id", "card-" + instructionCount);

    var instCardHeader = document.createElement("div");
    instCardHeader.setAttribute("class", "card-header");
    instCardHeader.appendChild(document.createTextNode("Instruction " + instructionCount));

    instli.appendChild(instCardHeader);

    var instDeleteBtn = document.createElement("button");
    instDeleteBtn.setAttribute("class", "btn btn-danger btn-sm");
    instDeleteBtn.setAttribute("type", "button");
    instDeleteBtn.innerHTML = "Delete";
    instDeleteBtn.addEventListener("click", deleteBtn, false);

    var instDelete = document.createElement("div");
    instDelete.setAttribute("class", "instruction-delete");
    instDeleteBtn.setAttribute("data-target-id", instructionCount);
    instDelete.appendChild(instDeleteBtn);

    instCardHeader.appendChild(instDelete);

    var instBody = document.createElement("div");
    instBody.setAttribute("class", "card-body");

    var instParagraph = document.createElement("p");
    instParagraph.innerHTML = instDesc;

    if (instImage != "") {
        var instImg = document.createElement("img");
        instImg.setAttribute("src", "data:image/png;base64, " + instImage);

        instBody.appendChild(instImg);
        instParagraph.setAttribute("class", "instruction-step");
    }

    instBody.appendChild(instParagraph);

    instli.appendChild(instBody);

    document.getElementById("target-instructions").appendChild(instli);
     
    /* add to instruction list */
    instructionDictionary[instructionCount] = {
        'img': instImage,
        'desc': instDesc
    };
    instructionCount++;
});

/**
 * This function deletes an instruction step.
 * @param {e} to capture click event
 */
var deleteBtn = function(e) {
    var instId = e.target.getAttribute("data-target-id");
    $("#card-" + instId).remove();

    /* remove from instruction dictionary */
    delete instructionDictionary[parseInt(instId, 10)];
    
    if (Object.keys(instructionDictionary).length == 0) {
        instructionCount = 1;
    }
};

/**
 * This function toggles the edit mode on and off.
 * @param {e} to capture click event
 */
$('#edit-mode-switch').on("click", function(e) {
    /* depends on current selected mode */
    if (selectedMode == 0 && 'x' in lastSquare) {
        selectedMode = 1;
        /* make map options visible in the right screen */
        $("#map-square-options").attr("class", "accordion-group");
    } else if (selectedMode == 0) {
        selectedMode = 1;
    } else {
        selectedMode = 0;
        $("#map-square-options").attr("class", "accordion-group d-none");
    }

});

/**
 * This function selects the map square type to use from the toolbox.
 * @param {e} to capture click event
 */
$('#toolbox button').on("click", function(e) {
    var mapType = e.target.getAttribute("data-map-type");
    var btnClass = e.target.getAttribute("class");

    /* set the style to reflect selection */
    $('#toolbox button[class="btn btn-primary"]').each(function(a, b) {
        b.setAttribute("class", "btn btn-secondary");
    });

    if (btnClass == "btn btn-primary") {
        selectedType = 0;
    } else {
        selectedType = parseInt(e.target.getAttribute("data-map-type"), 10);
        e.target.setAttribute("class", "btn btn-primary");
    }
});

/**
 * This function modifies the grid with selected map square from toolbox.
 * @param {e} to capture click event
 */
var gridCellFn = function(e) {
    var x = parseInt(e.target.getAttribute("x"));
    var y = parseInt(e.target.getAttribute("y"));
    
    /* if edit mode off, then just change panel to selected map square */
    if (!('x' in lastSquare) && selectedMode == 1) {
        $("#map-square-options").attr("class", "accordion-group");
    }
    
    /* otherwise, we change the map square*/
    lastSquare['x'] = x;
    lastSquare['y'] = y;

    $("#last-map-square").html("[" + x + ", " + y + "]");
    
    if ([x, y] in grid) {
        $("#map-type-selection").val(grid[[x, y]]);
    } else {
        $("#map-type-selection").val(0);
    }
    
	if ([x, y] in gridIdentifiers) {
		$("#square-identifier").val(gridIdentifiers[[x, y]]);
	} else {
	    $("#square-identifier").val("");
	}
	
	if ([x, y] in gridDirections) {
		$("#map-direction-selection").val(gridDirections[[x, y]]);
	} else {
		$("#map-direction-selection").val("");
	}

    /* exit if not edit mode */
    if (selectedMode == 1) {
        return;
    }
    
    if (startPosition['x'] == x && startPosition['y'] == y) {
    	startPosition = {}
    } else if (endPosition['x'] == x && endPosition['y'] == y) {
    	endPosition = {}
    }
    
    /* set start and end if selected type */
    if (selectedType == 1) {
        startPosition['x'] = x;
        startPosition['y'] = y;
    } else if (selectedType == 2) {
    	endPosition['x'] = x;
    	endPosition['y'] = y;
    }

    e.target.innerHTML = mapTypes[selectedType];

    if (selectedType != 0) {
        grid[[x, y]] = selectedType;
        $("#map-type-selection").val(grid[[x, y]]);
    } else {
        delete grid[[x, y]];
    }
};

/* make all grid tiles attached to the edit map square function */
$('div[class="grid-tile"]').on("click", gridCellFn);

/**
 * This function generates new maps based on a given size.
 * @param {x} the number of columns
 * @param {y} the number of rows
 */
var generateGrid = function(x, y) {
    $("#grid-maker-view").html("");

    for (var i = y - 1; i >= 0; i--) {
        var row = document.createElement("div");
        row.setAttribute("class", "grid-row");
        var col = document.createElement("div");
        col.setAttribute("class", "grid-tile-col text-muted");
        col.innerHTML = i;
        row.appendChild(col);

        for (var j = 0; j < x; j++) {
            var col = document.createElement("div");
            col.setAttribute("class", "grid-tile");
            col.setAttribute("x", j);
            col.setAttribute("y", i);
            col.addEventListener("click", gridCellFn, false);
            row.appendChild(col);
        }
        $("#grid-maker-view").append(row);
    }

    var row = document.createElement("div");
    row.setAttribute("class", "grid-row");
    var col = document.createElement("div");
    col.setAttribute("class", "grid-tile-col");
    row.appendChild(col);

    for (var j = 0; j < x; j++) {
        var col = document.createElement("div");
        col.setAttribute("class", "grid-tile-row text-muted");
        col.innerHTML = j;
        row.appendChild(col);
    }
    $("#grid-maker-view").append(row);

    startPosition = {};
    endPosition = {};
    lastSquare = {};
    grid = {};

    $("#map-square-options").attr("class", "accordion-group d-none");
};

/**
 * This function allows the set grid size button to work.
 * @param {e} the click event
 */
$("#map-size-set").on("click", function(e) {
    var prompt_clear = confirm("Would you like to clear the grid?");
    if (!prompt_clear) {
        return;
    }

    var x = parseInt($("#map-size-row").val(), 10);
    var y = parseInt($("#map-size-col").val(), 10);

    generateGrid(x, y);
});

/**
 * This function collapses the panels on the right screen.
 * @param {e} the click event
 */
$("#collapseTwo").on("show.bs.collapse", function(e){
	var x = lastSquare['x'];
	var y = lastSquare['y'];
	
	if ([x, y] in gridIdentifiers) {
		$("#square-identifier").val(gridIdentifiers[[x, y]]);
	}
	
	if ([x, y] in gridDirections) {
		$("#map-direction-selection").val(gridDirections[[x, y]]);
	}
});

/**
 * This function initiates compilation, by performing reachability,
 * then parsing of action scripts, and finally JSON serializing.
 * @param {e} the click event
 */
$("#compile-btn").on("click", function(e) {
	if (compilingInProgress) {
		return;
	}
	
	this.disabled = true;
	compilingInProgress = true;
	
	var grid_copy = grid;
	var gridIdentifiers_copy = gridIdentifiers;
	var gridDirections_copy = gridDirections;
	
	var reachable = [];
	var bad_path = false;
	
	/* dfs based reachability with modifications */
	var queue = [];
	consoleOutput = [];
	
	if (startPosition['x'] + "," + startPosition['y'] in grid_copy
	    && endPosition['x'] + "," + endPosition['y'] in grid_copy) {
		queue.push(startPosition['x'] + "," + startPosition['y']);
		reachable.push(startPosition['x'] + "," + startPosition['y']);
	} else {
		bad_path = true;
	}
		
	while (queue.length > 0) {		
		if (bad_path) {
			break;
		}
		
		var tuple = queue.pop();
				
		if (tuple in gridDirections_copy && gridDirections_copy[tuple] != 0) {	
		    /* modification to only consider the direction specified */
		    var tuple_temp = JSON.parse("[" + tuple + "]");

			switch (gridDirections_copy[tuple_temp]) {
				case "1":
					tuple_temp[1] += 1;
					break;
				case "2":
				    tuple_temp[0] -= 1;
					break;
				case "3":
				    tuple_temp[1] -= 1;
					break;
				case "4":
				    tuple_temp[0] += 1;
					break;
			}
									
			if (!([tuple_temp[0], tuple_temp[1]] in grid_copy)) {
				bad_path = true;
			} else if ([tuple_temp[0], tuple_temp[1]] in grid_copy
				    && !(reachable.includes(tuple_temp[0] + "," + tuple_temp[1]))
				    && grid_copy[[tuple_temp[0], tuple_temp[1]]] in [1,2,3]) {
				reachable.push(tuple_temp[0] + "," + tuple_temp[1]);
				queue.push(tuple_temp[0] + "," + tuple_temp[1]);
			}
		} else { /* for generic roads */
			var vectors = [[0,1], [-1,0], [0,-1], [1, 0]];
			
			/* check ever direction */
			for (var i = 0; i < vectors.length; i++) {
			    var tuple_temp = JSON.parse("[" + tuple + "]");
				var v = vectors[i];
				
				tuple_temp[0] = tuple_temp[0] + v[0];
				tuple_temp[1] = tuple_temp[1] + v[1];
				
				if ([tuple_temp[0], tuple_temp[1]] in grid_copy
				    && !(reachable.includes(tuple_temp[0] + "," + tuple_temp[1]))
				    && [1,2,3].includes(grid_copy[[tuple_temp[0], tuple_temp[1]]])) {
					reachable.push(tuple_temp[0] + "," + tuple_temp[1]);
					queue.push(tuple_temp[0] + "," + tuple_temp[1]);
				}
			}
		}
	}
	
	if (!(reachable.includes(startPosition['x'] + "," + startPosition['y'])
	    && reachable.includes(endPosition['x'] + "," + endPosition['y']))) {
		bad_path = true;
	} else if (Object.keys(grid_copy).length != reachable.length) {
		bad_path = true;
	}
		
	if (bad_path) {
		consoleOutput.push("Path is not reachable.");
	} else {
		consoleOutput.push("Path is reachable, compiling.");
	}
	
	if (bad_path) {
		exit(this);
		return;
	}
	
	var parseOutput = "";
	try {
		parseOutput = peg.parse(editorGlobal.getValue());
		consoleOutput.push("Parsing was succesful, serializing object..");
	} catch(error) {
		consoleOutput.push("Parsing error: " + error);
		exit(this);
		return;
	}
	
	var jsonString = JSON.stringify({
		instructions: instructionDictionary,
		grid: grid,
		gridIdentifiers: gridIdentifiers,
		gridDirections: gridDirections,
		gridParsed: parseOutput
		
	})
	
	consoleOutput.push("JSON object: " + jsonString.length + " bytes");
	
	consoleOutput.push("Done. Ready to save.");
	
	exit(this);
	function exit(a) {
		a.disabled = false;
		compilingInProgress = false;
		consoleView.refresh();
        consoleView.setValue(consoleOutput.join("\n"));
	}
});

/**
 * This function handles changes to map type selection in the map square options panel.
 * @param {e} the click event
 */
$("#map-type-selection").on("change", function(e) {
    if (lastSquare['x'] == startPosition['x'] && lastSquare['y'] == startPosition['y']) {
    	startPosition = {};
    } else if (lastSquare['x'] == endPosition['x'] && lastSquare['y'] == endPosition['y']) {
    	endPosition = {};
    }
    
    grid[[lastSquare['x'], lastSquare['y']]] = e.target.value;
    $('div[x="' + lastSquare['x'] + '"][y="' + lastSquare['y'] + '"]').html(mapTypes[e.target.value]);
});

/**
 * This function handles changes to the map square identifiers.
 * @param {e} the click event
 */
$("#square-identifier").on("change", function(e) {
    gridIdentifiers[[lastSquare['x'], lastSquare['y']]] = e.target.value;
});

/**
 * This function handles changes to the map square directions.
 * @param {e} the click event
 */
$("#map-direction-selection").on("change", function(e) {
    gridDirections[[lastSquare['x'], lastSquare['y']]] = e.target.value;
});

/**
 * This function handles changes to the lesson type.
 * @param {e} the click event
 */
$("#grid-type-selection").on("change", function(e) {
    lesson['type'] = parseInt(e.target.value, 10);
});

/**
 * This function handles changes to the lesson identifier.
 * @param {e} the click event
 */
$("#map-identifier-lesson").on("change", function(e) {
    lesson['identifier'] = e.target.value;
});

/**
 * This function handles changes to the required lesson.
 * @param {e} the click event
 */
$("#map-required-lesson").on("change", function(e) {
    lesson['required'] = e.target.value;
});

/**
 * This function handles changes to whether or not to automatically generate an exam.
 * @param {e} the click event
 */
$("#auto-generate-exam").on("change", function(e) {
    lesson['autoexam'] = e.target.checked ? 1 : 0;
});

/**
 * This function handles changes to whether the lesson is a draft or app ready.
 * @param {e} the click event
 */
$("#draft-mode-switch").on("change", function(e) {
    lesson['draft'] = e.target.checked ? 1 : 0;
});

/**
 * This function handles changes to the lesson image.
 * @param {e} the click event
 */
$("#lesson-image").on("change", function(e) {
    lesson['img'] = e.target.value;
});

generateGrid(8, 8);