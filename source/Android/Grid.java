package drivarsed.com.drivarsed.grid;

/**
* Stores a grid map as a 2d array.
*
* @author  Varshan Srivasanthakumar
* @version 1.0
* @since   2018-11-25
*/
public class Grid {
    int startX;
    int startY;
    int endX;
    int endY;

    Square[][] entireGrid;

    public Grid(Square[][] entireGrid, int sX, int sY, int tX, int tY) {
        this.entireGrid = entireGrid;
        this.startX = sX;
        this.startY = sY;
        this.endX = tX;
        this.endY = tY;
    }
}
