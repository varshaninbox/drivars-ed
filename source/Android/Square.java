package drivarsed.com.drivarsed.grid;

enum BlockType{
    START,
    STOPSIGN,
    END,
    UP,
    DOWN,
    RIGHT,
    LEFT,
    STREETLIGHT
}

/**
* Stores a grid square, along with instructional text and actions.
*
* @author  Varshan Srivasanthakumar
* @version 1.0
* @since   2018-11-25
*/
public class Square {
    BlockType squareType;
    String text;
    Double v;
    Double t;
    Boolean neg;

    Square() { }

    /**
    * This method sets the map square type.
    *
    * @param type The map square type as BlockType
    */
    public void setSquareType(BlockType type) {
        squareType = type;
    }

    /**
    * This method returns the type of map square.
    *
    * @return The BlockType of the map square.
    */
    public BlockType getSquareType() {
        return squareType;
    }

    /**
    * This method sets the action of the map square.
    */
    public void setAction(Double velocity, Double time, Boolean negation) {
        v = velocity;
        t = time;
        neg = negation;
    }

    /**
    * This method remove actions.
    */
    public void removeAction() {}

    /**
    * This method returns the instruction text stored in a map square.
    *
    * @return The instruction text of the map square.
    */
    public String getText() {
        return text;
    }

    /**
    * This method sets the instructional text for the map square.
    *
    * @param type The map square intructional text.
    */
    public void setText(String text) {
        this.text = text;
    }

    /**
    * This method remove instructional text.
    */
    public void removeText() {
        this.text = null;
    }
}
