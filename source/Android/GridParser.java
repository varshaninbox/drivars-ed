package drivarsed.com.drivarsed.grid;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
* Deserializes a JSON string into a Grid object.
*
* @author  Varshan Srivasanthakumar
* @version 1.0
* @since   2018-11-25
*/
public class GridParser {
    public static Grid g;
    
    /**
    * This method deserializes a JSON string into the Grid object g.
    *
    * @param text The JSON string
    */
    public static void parse(String text) {
        /* Stores the entire grid as a 2d array */
        Square[][] entireGrid = null;
        
        /* Starting and ending positions */
        int startingX = -1;
        int startingY = -1;
        int terminatingX = -1;
        int terminatingY = -1;

        try {
            JSONObject reader = new JSONObject(text);
            JSONArray grid = reader.getJSONArray("grid");

            /* Iterate over each grid row */
            for (int i = 0; i < grid.length(); i++) {
                JSONArray gridi = (JSONArray) grid.get(i);
                
                /* Check if grid array is initialized */
                if (entireGrid == null) {
                    entireGrid = new Square[grid.length()][gridi.length()];
                }
                
                /* Iterate over each grid column */
                for (int j = 0; j < gridi.length(); j++) {

                    JSONObject gridObj = (JSONObject) gridi.get(j);
                    Square newSquare = new Square();

                    /* Store the text in grid square object */
                    if (gridObj.has("block_text")) {
                        String gridObjText = (String) gridObj.get("block_text");
                        newSquare.setText(gridObjText);
                    }

                    /* Store the action in grid square object */
                    if (gridObj.has("action")) {
                        Double gridObjVelocity= (Double) gridObj.get("velocity");
                        Double gridObjTime = (Double) gridObj.get("interval");
                        Boolean gridObjNegation = (Boolean) gridObj.get("negation");

                        newSquare.setAction(gridObjVelocity, gridObjTime, gridObjNegation);
                    }

                    /* Unmarshalling grid squares */
                    if (gridObj.has("i")) {
                        String gridObjType = (String) gridObj.get("i");

                        switch (gridObjType) {
                            case "1":
                                newSquare.setSquareType(BlockType.DOWN);
                                break;
                            case "2":
                                newSquare.setSquareType(BlockType.RIGHT);
                                break;
                            case "3":
                                newSquare.setSquareType(BlockType.UP);
                                break;
                            case "4":
                                newSquare.setSquareType(BlockType.LEFT);
                                break;
                            case "5":
                                newSquare.setSquareType(BlockType.START);
                                startingY = i;
                                startingX = j;
                                break;
                            case "6":
                                newSquare.setSquareType(BlockType.END);
                                terminatingY = i;
                                terminatingX = j;
                                break;
                            case "7":
                                newSquare.setSquareType(BlockType.STREETLIGHT);
                                break;
                            case "8":
                                newSquare.setSquareType(BlockType.STOPSIGN);
                                break;
                        }
                    }

                    entireGrid[i][j] = newSquare;
                }
            }

        } catch (JSONException e) {
            Log.e("drivarsedgrid", e.toString());
        }

        /* Create grid object */
        if (startingX != -1 && terminatingX != -1) {
            g = new Grid(entireGrid, startingX, startingY, terminatingX, terminatingY);
        }

    }

}
