//
//  Session.swift
//  DrivAR's Ed
//
//  Created by Varshan Srivasanthakumar on 2019-02-12.
//  MIT License.
//

import Foundation
import ARKit

/* Provides a singleton as shared instance of Session */
let sharedSession = Session()

class Session {
    var grid = Grid() /* store current parsed grid */
    var targetHitTest: ARHitTestResult? /* contains hit test for debugging */
    var targetDelegate: ARSCNView? /* the ARKit session */
    var debugPlanes: [SCNNode] = [] /* planes for setting up session */
    var loadedView: Bool = false /* used to synchronize with view controller */
    var mapNode: SCNNode? = nil /* stores the map grid */
    var planeSize = 0.75 /* plane size, default is small */
    var selectedLessonID = 0 /* lesson to load */
    var startPositionVector: SCNVector3? /* initial start position for distance calculations */
    
    var lessons: [String] = [] /* lesson list */
    var lessonDescription: [String] = [] /* list of lesson descriptions */
    var lessonIDs: [Int] = [] /* list of lesson ids */
    
    var startPosition = Position()
    var endPosition = Position()
    
    var label: UITextView?
    var showMsg: ((String, UITextView, Double?) -> Void) = {_,_,_ in }
    
    /// Used to store positions
    struct Position {
        var x = 0
        var y = 0
    }
    
    /// Sends an HTTP request to load specific lessons
    func loadMap(hitTest: ARHitTestResult, arkitDelegate: ARSCNView) {
        JSONParser.get(requestID: "lesson/" + String(selectedLessonID))
        targetHitTest = hitTest
        targetDelegate = arkitDelegate
    }
    
    /// Removes debugging planes after a detected plane is selected.
    func removeDebugPlanes() {
        for debugPlaneNode in self.debugPlanes {
            debugPlaneNode.removeFromParentNode()
        }
        self.debugPlanes = []
    }
    
    /// Makes a road plane to display in the scene
    func makeRoadNode(x: CGFloat, y: CGFloat, z: CGFloat, material: SCNMaterial) -> SCNNode {
        let outerFloorNode = SCNPlane(width: CGFloat(planeSize), height: CGFloat(planeSize))
        outerFloorNode.materials = [material]
        
        /* create scene node */
        let planeNode = SCNNode(geometry: outerFloorNode)
        planeNode.position = SCNVector3(x,y,z)
        planeNode.eulerAngles.x = -.pi / 2
        
        return planeNode
    }
    
    /// Makes a stop sign node to display in the scene
    func makeStopSign(x: CGFloat, y: CGFloat, z: CGFloat) -> SCNNode {
        let material = SCNMaterial()
        material.diffuse.contents = UIColor.blue
        material.transparency = 0.5
        
        /* loads 3d asset */
        guard let stopSignNode = SCNScene(named: "SCNKitAssets.scnassets/stopSign.scn"), let stopSign3D = stopSignNode.rootNode.childNode(withName: "stopSign", recursively: true) else { return makeRoadNode(x: x, y: y, z: z, material: material) }
        stopSign3D.position = SCNVector3(x, y, z)
        return stopSign3D
    }
    
    /// Generates planes based on loaded grid map
    func generatePlanes() -> SCNNode {
        let gridMap = SCNNode()
        
        var i = 0
        for grid in grid.gridMap {
            var j = 0
            
            /* cStyledCounter to iterate each grid object */
            var cStyledCounter = 0
            while cStyledCounter < grid.gridMap.count {
                let mapSquare = grid.gridMap[cStyledCounter]
                if (mapSquare.squareType != 0) {
                    var squareNode: SCNNode?
                    /* for start square */
                    if mapSquare.squareType == 5 {
                        let material = SCNMaterial()
                        material.diffuse.contents = UIImage(named: "road.png")
                        material.transparency = 0.5
                        squareNode = makeRoadNode(x: 0, y: 0, z: 0, material: material)
                        
                        /* loads image node overlay */
                        let material_place = SCNMaterial()
                        material_place.diffuse.contents = UIImage(named: "start.png")
                        material_place.transparency = 1
                        
                        let plane_init = SCNPlane(width: CGFloat(planeSize)/2, height: CGFloat(planeSize)/2)
                        plane_init.materials = [material_place]
                        
                        let planeInit = SCNNode(geometry: plane_init)
                        squareNode!.addChildNode(planeInit)
                    } else if mapSquare.squareType == 6 {
                        /* for end square */
                        let material = SCNMaterial()
                        material.diffuse.contents = UIImage(named: "road.png")
                        material.transparency = 0.5
                        squareNode = makeRoadNode(x: 0, y: 0, z: 0, material: material)
                        
                        /* loads end image overlay */
                        let material_place = SCNMaterial()
                        material_place.diffuse.contents = UIImage(named: "goal.png")
                        material_place.transparency = 1
                        
                        let plane_init = SCNPlane(width: CGFloat(planeSize)/2, height: CGFloat(planeSize)/2)
                        plane_init.materials = [material_place]
                        
                        let planeInit = SCNNode(geometry: plane_init)
                        squareNode!.addChildNode(planeInit)
                    } else if mapSquare.squareType == 7 {
                        /* for stop sign square */
                        squareNode = makeStopSign(x: 0, y: 0, z: 0)
                    } else {
                        /* for all road squares */
                        let material = SCNMaterial()
                        material.diffuse.contents = UIImage(named: "road.png")
                        material.transparency = 0.5
                        squareNode = makeRoadNode(x: 0, y: 0, z: 0, material: material)
                    }
                    
                    /* Adjust grid map position so that start square is in front of us */
                    squareNode!.position = SCNVector3(CGFloat(planeSize * Double(j)), -1 * 0.5, -CGFloat(planeSize * Double(sharedSession.startPosition.x - i) + 1))
                    sharedMatrix.append(entry: GridMatrixEntry(vector: simd_float3(squareNode!.transform.m41, squareNode!.transform.m42, squareNode!.transform.m43), node: squareNode!, nodeType: mapSquare))
                    gridMap.addChildNode(squareNode!)
                    
                    j += 1
                }
                cStyledCounter += 1
            }
            i += 1
        }
        gridMap.position = SCNVector3(0, 0, 0)
        
        return gridMap
    }
    
    /// Sets up the loaded grid map into the scene.
    func updateMap() {
        if !loadedView {
            removeDebugPlanes()
            targetDelegate!.scene.rootNode.enumerateChildNodes { (existingNode, _) in
                existingNode.removeFromParentNode()
            }
            
            self.mapNode = self.generatePlanes()
            self.mapNode!.position = SCNVector3(targetHitTest!.worldTransform.columns.3.x, targetHitTest!.worldTransform.columns.3.y, targetHitTest!.worldTransform.columns.3.z)
            guard let frame = targetDelegate!.session.currentFrame else {
                return
            }
            
            /* Align the camera and the grid map */
            self.mapNode!.eulerAngles.y = frame.camera.eulerAngles.y
            targetDelegate!.scene.rootNode.addChildNode(self.mapNode!)
            loadedView = true            
        }
    }
}
