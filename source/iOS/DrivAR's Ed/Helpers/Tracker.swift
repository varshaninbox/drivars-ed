//
//  Tracker.swift
//  DrivAR's Ed
//
//  Created by Varshan Srivasanthakumar on 2019-02-10.
//  MIT License.
//

import Foundation
import ARKit

let sharedTracker = Tracker()

class Tracker {
    var last: GridMatrixEntry?
    var lessonFail: Bool = false /* to store if the lesson failed */
    var lessonBegun: Bool = false /* to store if the lesson begun */
    
    /// Called on each session update, controls which map square has it's actions evaluated.
    func track() {
        if let lastEntry = last {
            let newLast = sharedMatrix.findMinDistance()!
            if lastEntry.nodeType.squareId != newLast.nodeType.squareId {
                newLast.nodeType.evaluate()
                last = newLast
            }
        } else {
            last = sharedMatrix.findMinDistance()!
            last!.nodeType.evaluate()
        }
    }
}

