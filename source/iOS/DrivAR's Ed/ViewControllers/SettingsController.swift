//
//  SettingsController.swift
//  DrivAR's Ed
//
//  Provides the view for settings.
//
//  Created by Varshan Srivasanthakumar on 2019-02-13.
//  MIT License.
//

import Foundation
import UIKit

class SettingsController: UITableViewController {
    @IBOutlet weak var roadSize: UISegmentedControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.title = "Settings"
    }
    
    /// Sets the road size if the setting changed.
    @IBAction func onRoadSizeChange(_ sender: Any) {
        switch (roadSize.selectedSegmentIndex) {
        case 0:
            sharedSession.planeSize = 0.75
        case 1:
            sharedSession.planeSize = 1
        case 2:
            sharedSession.planeSize = 1.5
        default:
            sharedSession.planeSize = 0.75
        }
    }
}
