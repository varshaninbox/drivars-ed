//
//  LessonsController.swift
//  DrivAR's Ed
//
//  Provides the view for the list of lessons.
//
//  Created by Varshan Srivasanthakumar on 2019-02-13.
//  MIT License.
//


import UIKit

/// Provides the layout of the lesson table cell.
class lessonCell: UITableViewCell {
    @IBOutlet weak var lessonName: UILabel!
    @IBOutlet weak var lessonDescription: UILabel!
    /* stores the lesson id */
    var lessonID = 0
}

/// Provides the view for the list of lessons.
class LessonsController: UITableViewController {
    /* allows for the list of lessons to be refreshed */
    @IBOutlet weak var refresher: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        JSONParser.get(requestID: "lessons", type: .lessons, completion: callback)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.title = "Lessons"
    }
    
    /// Pulls the list of lessons from the server again.
    @IBAction func refresh(_ sender: Any) {
        JSONParser.get(requestID: "lessons", type: .lessons, completion: callback)
        refresher.endRefreshing()
    }
    
    /// Callback from refresh() will reload the table view if the refresh was successful.
    func callback(completed: Bool) {
        if (completed) {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sharedSession.lessons.count
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Lessons"
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "lessonCell", for: indexPath) as! lessonCell
        
        cell.lessonName.text = sharedSession.lessons[indexPath.row]
        cell.lessonDescription.text = sharedSession.lessonDescription[indexPath.row]
        cell.lessonID = sharedSession.lessonIDs[indexPath.row]

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        sharedSession.selectedLessonID = sharedSession.lessonIDs[indexPath.row]
    }
}
