//
//  ARScreenController.swift
//  DrivAR's Ed
//
//  Provides the main scene view for selected lessons.
//
//  Created by Varshan Srivasanthakumar on 2019-02-07.
//  MIT License.
//

import UIKit
import ARKit
import SceneKit

class ARScreenController: UIViewController, ARSCNViewDelegate, ARSessionDelegate {
    @IBOutlet weak var label: UITextView!
    
    @IBOutlet weak var arkitDelegate: ARSCNView!
    var numberOfTaps = 0 /* Used to count the number of taps */
    var alreadyRendered = false /* Used to check if scene already rendered */
    var sessionCounter = 1 /* Keeps track of number of calls to session() */
    
    /// Sets up the intial ARKit view upon load.
    override func viewDidLoad() {
        super.viewDidLoad()

        /* Set the view's delegate */
        arkitDelegate.delegate = self
        arkitDelegate.session.delegate = self
        
        /* Show statistics such as fps and timing information */
        arkitDelegate.showsStatistics = true
        
        /* Add gesture recognition for taps */
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapped))
        arkitDelegate.addGestureRecognizer(gestureRecognizer)
        
        /* Display initial message */
        showMessage(message: "Find an open space then tap to continue.", label: label)
    }

    /// Sets up the intial ARKit view upon load.
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        /* Set up to detect horizontal planes */
        setUpSceneView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sharedSession.loadedView = false
        arkitDelegate.session.pause()
    }
    
    /// Sets up ARKit to detect horizontal planes.
    func setUpSceneView() {
        /* Set up to detect horizontal planes */
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = .horizontal
        arkitDelegate.session.run(configuration)
    }
    
    /// Called every few milliseconds while the AR session is active,
    /// whenever new camera information is available.
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        /* Compute the distances to grid squares every 30 session calls */
        sessionCounter += 1
        if sessionCounter == 30 {
            sessionCounter = 0

            /* If the grid is loaded, compute the distance and velocity,
             * based on the updates to the camera position.
             */
            if sharedSession.loadedView {
                sharedMatrix.updateDistances(vector: simd_float3(frame.camera.transform.columns.3.x, frame.camera.transform.columns.3.y, frame.camera.transform.columns.3.z))
                
                sharedMatrix.updateVelocity(vector: simd_float3(frame.camera.transform.columns.3.x, frame.camera.transform.columns.3.y, frame.camera.transform.columns.3.z))

                /* Call track() to execute map square commands */
                sharedTracker.track()
            }
        }
    }
    
    /// Called when AR anchors are added to the SceneKit scene.
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        /* If the grid is not rendered, find plane to place grid on */
        if !alreadyRendered {
            /* Get plane anchors only */
            guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
            
            /* set the width, height and geometry of the plane */
            let width = CGFloat(sharedSession.planeSize * 0.5)
            let height = CGFloat(sharedSession.planeSize * 0.5)
            let plane = SCNPlane(width: width, height: height)
            
            /* Create material for plane text (image says START HERE) */
            let material_place = SCNMaterial()
            material_place.diffuse.contents = UIImage(named: "init.png")
            material_place.transparency = 0.5
            
            /* Create material for road texture */
            let material = SCNMaterial()
            material.diffuse.contents = UIImage(named: "road.png")
            material.transparency = 0.5
            
            plane.materials = [material]
            
            /* set up the planes (one for text overlay and one for the road) */
            let plane_init = SCNPlane(width: width/2, height: height/2)
            plane_init.materials = [material_place]
            
            /* text overlay plane */
            let planeInit = SCNNode(geometry: plane_init)
            
            /* road plane */
            let planeNode = SCNNode(geometry: plane)
            
            /* place the plane at ar plane anchor location */
            let x = CGFloat(planeAnchor.center.x)
            let y = CGFloat(planeAnchor.center.y)
            let z = CGFloat(planeAnchor.center.z)
            
            planeNode.position = SCNVector3(x,y,z)
            planeNode.eulerAngles.x = -.pi / 2
            
            /* place the plane on anchor */
            planeInit.position = SCNVector3(x,y,z)
            planeNode.addChildNode(planeInit)
            node.addChildNode(planeNode)
            
            /* Attach plane to plane list to remove later */
            sharedSession.debugPlanes.append(node)
            
            /* Only render this part once */
            alreadyRendered = true
            
            /* set up the shared session to display text onto the label */
            sharedSession.showMsg = self.showMessage
            sharedSession.label = self.label
                
            DispatchQueue.main.async {
                self.showMessage(message: "A plane has been found! Tap the plane to continue.", label: self.label)
            }
        }
    }
    
    /// Gesture handle function for taps
    @objc func tapped(gesture: UITapGestureRecognizer) {
        /* If the first tap */
        if numberOfTaps == 0 {
            /* begin the plane detection */
            arkitDelegate.delegate = self
            arkitDelegate.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
            
            showMessage(message: "Wait for a plane to appear.", label: label)
            numberOfTaps += 1
            return
        } else if (numberOfTaps == 1) {
            /* check if tap hit a plane */
            let touchPosition = gesture.location(in: arkitDelegate)
            let hitTestResults = arkitDelegate.hitTest(touchPosition, types: .existingPlane)
            
            guard let hitTest = hitTestResults.first else {
                return
            }
            
            /* eliminate the debugging planes */
            arkitDelegate.debugOptions = []
            showMessage(message: "Anchor found. Loading the map..", label: label, seconds: 5)

            /* load the grid map since hit test was successful */
            sharedSession.loadMap(hitTest: hitTest, arkitDelegate: arkitDelegate)
            numberOfTaps += 1
        }
    }
    
    /// Displays message on the view controller.
    func showMessage(message: String, label: UITextView, seconds: Double? = nil) {
        label.text = message
        label.alpha = 1
        
        if seconds != nil {
            /* Delete message if it expires after some time */
            DispatchQueue.main.asyncAfter(deadline: .now() + seconds!) {
                if label.text == message {
                    label.text = ""
                    label.alpha = 0
                }
            }
        }
    }
}

