//
//  JSONParserErrors.swift
//  DrivAR's Ed
//
//  Created by Varshan Srivasanthakumar on 2019-02-13.
//  MIT License.
//

import Foundation

enum JSONParserErrors: Error {
    case downloadFailed
    case cannotReadGrid
    case cannotReadJSON
}
