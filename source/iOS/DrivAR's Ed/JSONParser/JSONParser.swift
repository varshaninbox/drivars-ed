//
//  JSONParser.swift
//  DrivAR's Ed
//
//  Loads JSON from the server and parses JSON strings.
//
//  Created by Varshan Srivasanthakumar on 2019-01-09.
//  MIT License.
//

import Foundation

class JSONParser {
    static var host = "http://ec2-3-84-36-239.compute-1.amazonaws.com/"
    
    /// Retrieves lesson lists and individual lessons, depending on specified parser type.
    /// @param type Used to specifiy the parser type.
    /// @param completion Closure or function to callback upon completion.
    static func get(requestID: String, type: JSONParserTypes = .grid, completion: ((Bool) -> ())? = nil) {
        guard let url = URL(string: host + requestID) else {return}
        
        /* Using shared URLSession, load requested URL asynchroniously */
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let dataResponse = data, error == nil else {
                print(error?.localizedDescription ?? "Failed to load lesson resource")
                return }
            do {
                if type == .grid {
                    /* call lesson map parser */
                    try JSONParser.parse(dataResponse: dataResponse)
                } else if type == .lessons {
                    /* call lesson list parser */
                    try JSONParser.parseLessons(dataResponse: dataResponse, completion: completion!)
                }
            } catch let parsingError {
                print("Error", parsingError)
            }
        }
        
        task.resume()
    }
    
    /// Parses lesson list from JSON response data.
    static func parseLessons(dataResponse: Data, completion: ((Bool)->())) throws {
        /* Parses reponse object from data */
        let jsonResponse = try JSONSerialization.jsonObject(with:
            dataResponse, options: [])
        
        /* reset the sharedsession arrays for lesson lists */
        sharedSession.lessonIDs = []
        sharedSession.lessonDescription = []
        sharedSession.lessons = []
        
        /* Parses the object received */
        if let dictionary = jsonResponse as? [Any] {
            var i = 0
            for lesson in dictionary {
                /* add each lesson to the list */
                if let lessonData = lesson as? [String: Any] {
                    if let id = lessonData["id"] as? Int {
                        sharedSession.lessonIDs.append(id)
                    }
                    if let description = lessonData["description"] as? String {
                        sharedSession.lessonDescription.append(description)
                    }
                    if let title = lessonData["title"] as? String {
                        sharedSession.lessons.append(title)
                    }
                }
                i += 1
            }
            
            completion(true)
        } else {
            /* parsing failure */
            completion(false)
        }
        
    }
    
    /// Parses individual lessons from JSON response data.
    static func parse(dataResponse: Data) throws {
        /* Parses reponse object from data */
        let jsonResponse = try JSONSerialization.jsonObject(with:
            dataResponse, options: [])
        
        /* Provides a unique id to each map square */
        var squareCount = 0
        
        /* Refresh grid */
        sharedSession.grid = Grid()
        
        /* Parsing through the JSON response object */
        if let dictionary = jsonResponse as? [String: Any] {
            if let grid = dictionary["grid"] as? [Any] {
                var i = 0
                
                /* Grid is stored as row first */
                for rowObject in grid {
                    if let row = rowObject as? [Any] {
                        var gridR = GridRow()
                        
                        var j = 0
                        /* Process each column object */
                        for gridSquareObject in row {
                            if let gridSquare = gridSquareObject as? [String: String] {
                                var sq = MapSquare()
                                
                                /* assign id */
                                sq.squareId = squareCount
                                squareCount += 1
                                
                                if let id = gridSquare["i"] {
                                    sq.squareType = Int(id)!
                                    
                                    /* square is start square */
                                    if id == "5" {
                                        sharedSession.startPosition.y = j
                                        sharedSession.startPosition.x = i
                                    }
                                    
                                    /* square is end square */
                                    if id == "6" {
                                        sharedSession.endPosition.y = j
                                        sharedSession.endPosition.x = i
                                    }
                                }
                                
                                /* square has text to display */
                                if let text = gridSquare["t"] {
                                    sq.squareText = text
                                }
                                
                                /* square has action to verify */
                                if gridSquare["a"] != nil {
                                    if let interval = gridSquare["r"] {
                                        sq.squareAction.interval = Int(interval)!
                                    }
                                    
                                    if let rate = gridSquare["s"] {
                                        sq.squareAction.velocity = Double(rate)!
                                    }
                                    
                                    if let negate = gridSquare["n"] {
                                        sq.squareAction.negation = Bool(negate) ?? false
                                    }
                                }
                                
                                gridR.gridMap.append(sq)
                            }
                            j += 1
                        }
                        
                        /* append column object to row */
                        sharedSession.grid.gridMap.append(gridR)
                    }
                    i += 1
                }
            } else {
                throw JSONParserErrors.cannotReadGrid
            }
        } else {
            throw JSONParserErrors.cannotReadJSON
        }
        
        /* call updateMap() to load the stored grid map */
        sharedSession.updateMap()
    }
}
