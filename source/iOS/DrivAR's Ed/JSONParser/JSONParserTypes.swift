//
//  JSONParserTypes.swift
//  DrivAR's Ed
//
//  Created by Varshan Srivasanthakumar on 2019-02-14.
//  MIT License.
//

import Foundation

enum JSONParserTypes {
    case grid
    case lessons
}
