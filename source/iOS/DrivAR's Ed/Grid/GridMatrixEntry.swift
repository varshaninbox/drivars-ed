//
//  GridMatrixEntry.swift
//  DrivAR's Ed
//
//  Created by Varshan Srivasanthakumar on 2019-03-11.
//  MIT License.
//

import Foundation
import ARKit

class GridMatrixEntry {
    var vector: simd_float3?
    var original_vector: simd_float3?
    var node: SCNNode?
    var distanceFromCamera: Double? = 0
    var distanceFromCamera3D: Double? = 0
    var nodeType: MapSquare
    var offRoad = false
    
    init(vector: simd_float3, node: SCNNode, nodeType: MapSquare, offRoad: Bool = false) {
        self.vector = vector
        self.node = node
        self.nodeType = nodeType
        self.offRoad = offRoad
    }
}
