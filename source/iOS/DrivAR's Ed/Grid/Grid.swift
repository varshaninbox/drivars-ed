//
//  Grid.swift
//  DrivAR's EdUITests
//
//  Created by Varshan Srivasanthakumar on 2019-02-12.
//  MIT License.
//

import Foundation

struct Grid {
    var gridMap:[GridRow] = [];
    
    /// Returns the number of rows in the grid.
    func rowSize() -> Int {
        return gridMap.count
    }
}
