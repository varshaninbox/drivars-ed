//
//  GridRow.swift
//  DrivAR's Ed
//
//  Created by Varshan Srivasanthakumar on 2019-02-12.
//  MIT License.
//

import Foundation

struct GridRow {
    var gridMap:[MapSquare] = [];
    
    /// Returns the number of columns in the row.
    func columnSize() -> Int {
        return gridMap.count
    }
}
