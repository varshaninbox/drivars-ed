//
//  MapSquare.swift
//  DrivAR's Ed
//
//  Created by Varshan Srivasanthakumar on 2019-02-12.
//  MIT License.
//

import Foundation
import ARKit

struct MapSquare {
    var squareType = 0
    var squareId = 0
    var squareText: String?
    struct Action {
        var velocity: Double?
        var interval: Int?
        var negation: Bool?
    }
    var squareAction = Action()
    var position = simd_float3(0,0,0)
    var alreadyEvaluated = false
    
    /// Evaluates the actions and displays the text of map squares when user is on the square.
    mutating func evaluate() {
        /* Begin once on the start square*/
        if squareType == 5 {
            sharedTracker.lessonBegun = true
        }
        
        /* If outside the start square and lesson hasn't begun */
        if !sharedTracker.lessonBegun {
            sharedSession.showMsg("Go to the start square.", sharedSession.label!, 3)
            return
        }
        
        /* If off road during lesson */
        if squareType == 0 && sharedTracker.lessonBegun {
            sharedSession.showMsg("You're off the road, get back on the road.", sharedSession.label!, 3)
            return
        }
        
        /* Pass only if lessonFail is still false as initially */
        if squareType == 6 {
            if sharedTracker.lessonFail {
                sharedSession.showMsg("You have failed.", sharedSession.label!, 3)
                return
            }
            sharedSession.showMsg("You have passed.", sharedSession.label!, 3)
            return
        }
        
        /* If the square was already evaluated.. */
        if alreadyEvaluated {
            sharedSession.showMsg("You're going the wrong way, go forward.", sharedSession.label!, 3)
            return
        } else {
            alreadyEvaluated = true
        }
        
        /* Display any text for square */
        if let t = squareText {
            sharedSession.showMsg(t, sharedSession.label!, 3)
        }
        
        /* Perform action, starting with negated actions */
        if let n = squareAction.negation {
            if let interval = squareAction.interval {
                /* set lesson to fail unless action performed*/
                sharedTracker.lessonFail = true
                if n {
                    /* run while squareId matches last square visited */
                    while (sharedTracker.last!.nodeType.squareId == squareId) {
                        /* begin timers */
                        let newTime = Date().timeIntervalSince1970
                        var currentTime = Date().timeIntervalSince1970
                        
                        /* boolean to break out of outer loop */
                        var done = false
                        
                        /* not equal velocity because negation */
                        while ((sharedMatrix.velocity.magnitude*100).rounded()/100 != squareAction.velocity) {
                            /* update timer */
                            currentTime = Date().timeIntervalSince1970
                            if Int(currentTime - newTime) > interval {
                                sharedTracker.lessonFail = false
                                done = true
                                sharedSession.showMsg("Action complete.", sharedSession.label!, 3)
                                break
                            }
                        }
                        
                        /* exit outer loop if done */
                        if (done) {
                            break
                        }
                        
                    }
                } else {
                    while (sharedTracker.last!.nodeType.squareId == squareId) {
                        /* begin timers */
                        let newTime = Date().timeIntervalSince1970
                        var currentTime = Date().timeIntervalSince1970
                        
                        /* boolean to break out of outer loop */
                        var done = false
                        
                        /* while player's velocity equals velocity */
                        while ((sharedMatrix.velocity.magnitude*100).rounded()/100 == squareAction.velocity) {
                            /* update timer */
                            currentTime = Date().timeIntervalSince1970
                            if Int(currentTime - newTime) > interval {
                                sharedTracker.lessonFail = false
                                done = true
                                sharedSession.showMsg("Action complete.", sharedSession.label!, 3)
                                break
                            }
                        }
                        
                        /* exit outer loop if done*/
                        if (done) {
                            break
                        }
                        
                    }
                }
            }
        }
        
        /* If the lesson is already failed */
        if (sharedTracker.lessonFail) {
            sharedSession.showMsg("You have failed.", sharedSession.label!, 3)
            return
        }
    }
}
