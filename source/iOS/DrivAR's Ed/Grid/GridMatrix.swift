//
//  GridMatrix.swift
//  DrivAR's Ed
//
//  Created by Varshan Srivasanthakumar on 2019-03-10.
//  MIT License.
//

import Foundation
import ARKit
import SceneKit

let sharedMatrix = GridMatrix()

struct Velocity {
    var last: simd_float3?
    var duration: Double?
    var lastTime: TimeInterval?
    var magnitude: Double = 0.0
}

class GridMatrix {
    var matrix: [GridMatrixEntry] = []
    var velocity = Velocity()
    
    /// Adds map square to track
    func append(entry: GridMatrixEntry) {
        matrix.append(entry)
    }
    
    /// Updates player's velocity based on provided vector.
    func updateVelocity(vector: simd_float3) {
        let newTime = Date().timeIntervalSince1970
        if let last = velocity.last {
            let magnitude = sqrt(pow(last.x - vector.x, 2) + pow(last.y - vector.y, 2) + pow(last.z - vector.z, 2))
            let difference = velocity.lastTime! - newTime
            velocity.magnitude = Double(magnitude)/difference
        } else {
            velocity.magnitude = 0
        }
        velocity.last = vector
        velocity.lastTime = newTime
    }
    
    /// Updates distances to all map squares.
    func updateDistances(vector: simd_float3) {
        var i = 0
        while i < matrix.count {
            /* Calculates the magnitude using pythagoras */
            matrix[i].distanceFromCamera = sqrt(pow(Double(matrix[i].vector!.x - vector.x), 2)
                                                 + pow(Double(matrix[i].vector!.z - vector.z), 2))
            
            matrix[i].distanceFromCamera3D = sqrt(pow(Double(matrix[i].vector!.x - vector.x), 2)
                                                   + pow(Double(matrix[i].vector!.y - vector.y), 2)
                                                   + pow(Double(matrix[i].vector!.z - vector.z), 2))
            i += 1
        }
    }
    
    /// Finds the map square with the smallest distance. Returns "off-road" square if none within < 0.55 * planeSize distance.
    func findMinDistance() -> GridMatrixEntry? {
        var i = 0
        var min: Double?
        var entry: GridMatrixEntry?
        
        while i < matrix.count {
            if min == nil {
                min = matrix[i].distanceFromCamera
                entry = matrix[i]
            } else if (matrix[i].distanceFromCamera! < min!) {
                min = matrix[i].distanceFromCamera
                entry = matrix[i]
            }
            i += 1
        }
        
        /* if min is nil, then grid is empty */
        if (min != nil) {
            /* check if out of bounds (meaning off road), with some rounding error*/
            if min! > sharedSession.planeSize * 0.55{
                let mapS = GridMatrixEntry(vector: simd_float3(0,0,0), node: SCNNode(), nodeType: MapSquare(), offRoad: true)
                return mapS
            }
        }
        
        return entry
    }
}
